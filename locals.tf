locals {
  DATABASE   = "database"
  PRODUCTION = "production"
  postgres   = data.vault_generic_secret.postgresql.data
  components_env = {
    for key, component in var.components : key => {
      for name, data in var.project.namespaces : name =>
      merge([
        for env_prefix, env_postfixes in data :
        {
          for postfix_key, postfix_data in env_postfixes :
          upper(join("_", [env_prefix, postfix_key])) => postfix_data if contains(component.env, env_prefix)
        }
      ]...)
    }
  }
  env_data = merge([
    for name, data in local.components_env : {
      for namespace, env_raw in data : "${namespace}/${name}" => env_raw
    }
  ]...)
}
