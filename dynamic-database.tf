resource "vault_mount" "storage" {
  path = "postgresql-storage"
  type = "database"
}

resource "helm_release" "postgresql-dynamic-secret" {
  name       = "postgresql-dynamic-secret"
  repository = "https://dysnix.github.io/charts"
  chart      = "raw"
  version    = "v0.3.2"
  values = [
    yamlencode({
      resources = [
        for key, data in var.project.namespaces :
        {
          apiVersion = "secrets.hashicorp.com/v1beta1"
          kind       = "VaultDynamicSecret"
          metadata = {
            name      = postgresql_database.project[key].name
            namespace = kubernetes_namespace.all_namespaces[key].metadata[0].name
          }
          spec = {
            path  = "creds/${vault_database_secret_backend_role.project[key].name}"
            mount = vault_mount.storage.path
            destination = {
              name   = postgresql_database.project[key].name
              create = true
            }
            rolloutRestartTargets = [
              for component_key, component_data in var.components :
              {
                kind = "Deployment"
                name = component_key
              } if contains(component_data.env, local.DATABASE)
            ]
            vaultAuthRef = key
          }
        }
      ]
    })
  ]
  depends_on = [helm_release.vault_configs]
}

resource "vault_database_secret_backend_role" "project" {
  for_each = var.project.namespaces
  name     = each.key
  backend  = vault_mount.storage.path
  db_name  = each.key
  creation_statements = [
    "CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}';",
    "GRANT ALL PRIVILEGES ON DATABASE \"${postgresql_database.project[each.key].name}\" TO \"{{name}}\";",
    "GRANT ALL PRIVILEGES ON SCHEMA public TO \"{{name}}\";",
    "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO \"{{name}}\";",
    "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO \"{{name}}\";",
  ]
  revocation_statements = [
    "REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM \"{{name}}\";",
    "REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM \"{{name}}\";",
    "REVOKE ALL PRIVILEGES ON SCHEMA public FROM \"{{name}}\";",
    "REVOKE ALL PRIVILEGES ON DATABASE \"${postgresql_database.project[each.key].name}\" FROM \"{{name}}\";",
  ]
  default_ttl = 300
  max_ttl     = 400
}

locals {
  postgresql = data.vault_generic_secret.postgresql.data
}

resource "vault_database_secret_backend_connection" "postgresql-dynamic" {
  for_each          = var.project.namespaces
  backend           = vault_mount.storage.path
  name              = each.key
  allowed_roles     = [vault_database_secret_backend_role.project[each.key].name]
  verify_connection = true
  postgresql {
    username       = "postgres"
    password       = local.postgresql["INITIAL_ADMIN_PASSWORD"]
    connection_url = "postgresql://{{username}}:{{password}}@${each.value.database.host}:${each.value.database.port}/${postgresql_database.project[each.key].name}"
  }
  depends_on = [vault_mount.storage]
}
