resource "helm_release" "vault_configs" {
  name       = "vaultconfigs"
  repository = "https://dysnix.github.io/charts"
  chart      = "raw"
  namespace  = "vault"
  version    = "v0.3.2"
  values = [
    yamlencode({
      resources = [
        for each, data in var.project.namespaces :
        {
          apiVersion = "secrets.hashicorp.com/v1beta1"
          kind       = "VaultAuth"
          metadata = {
            name      = each
            namespace = kubernetes_namespace.all_namespaces[each].metadata[0].name
          }
          spec = {
            method = vault_auth_backend.kubernetes.type
            mount  = vault_auth_backend.kubernetes.path
            kubernetes = {
              role           = vault_kubernetes_auth_backend_role.user_namespace_auth_roles[each].role_name
              serviceAccount = kubernetes_service_account.user_service_accounts[each].metadata[0].name
              audiences = [
                "vault"
              ]
            }
          }
        }
      ]
    })
  ]
}

locals {
  refresh = "30s"
}

resource "helm_release" "my-flora-env" {
  name       = "appenv"
  repository = "https://dysnix.github.io/charts"
  chart      = "raw"
  namespace  = "vault"
  version    = "v0.3.2"
  values = [
    yamlencode({
      resources = [
        for each, data in local.env_data :
        {
          apiVersion = "secrets.hashicorp.com/v1beta1"
          kind       = "VaultStaticSecret"
          metadata = {
            name      = "app-${replace(each, "/", "-")}"
            namespace = kubernetes_namespace.all_namespaces[split("/", each)[0]].metadata[0].name
          }
          spec = {
            type  = "kv-v2"
            path  = each
            mount = vault_mount.project.path
            destination = {
              name   = split("/", each)[1]
              create = true
            }
            refreshAfter = local.refresh
            vaultAuthRef = split("/", each)[0]
          }
        }
      ]
    })
  ]
  depends_on = [vault_kv_secret_v2.project]
}

resource "vault_mount" "transit" {
  path = "transit"
  type = "transit"
}

resource "vault_transit_secret_cache_config" "cfg" {
  backend = vault_mount.transit.path
  size    = 500
}

resource "vault_transit_secret_backend_key" "key" {
  backend = vault_mount.transit.path
  name    = "vso-client-cache"
}

resource "vault_policy" "auth-policy-operator" {

  name       = "operator"
  policy     = <<EOF
path "transit/encrypt/vso-client-cache" {
   capabilities = ["create", "update"]
}
path "transit/decrypt/vso-client-cache" {
   capabilities = ["create", "update"]
}
EOF
  depends_on = [kubernetes_service_account.user_service_accounts]
}

data "kubernetes_service_account" "vault-secrets-operator-controller-manager" {
  metadata {
    namespace = "vault"
    name      = "vault-secrets-operator-controller-manager"
  }
}

resource "kubernetes_secret_v1" "vault-secrets-operator-controller-manager" {
  metadata {
    name      = "vault-secrets-operator-controller-manager-token-g955r"
    namespace = data.kubernetes_namespace_v1.vault.metadata[0].name
    annotations = {
      "kubernetes.io/service-account.name" = data.kubernetes_service_account.vault-secrets-operator-controller-manager.metadata[0].name
    }
  }
  type = "kubernetes.io/service-account-token"
}

resource "kubernetes_secret" "vault-secrets-operator-controller-manager-token" {
  metadata {
    name      = data.kubernetes_service_account.vault-secrets-operator-controller-manager.metadata[0].name
    namespace = data.kubernetes_namespace_v1.vault.metadata[0].name

    annotations = {
      "kubernetes.io/service-account.name" = data.kubernetes_service_account.vault-secrets-operator-controller-manager.metadata[0].name
    }
  }

  type = "kubernetes.io/service-account-token"
}

locals {
  token_policies = [
    for username, data in var.developers :
    "user-policy-${username}"
  ]
}

resource "vault_kubernetes_auth_backend_role" "operator" {
  role_name                        = "auth-role-operator"
  backend                          = vault_auth_backend.kubernetes.path
  audience                         = "vault"
  token_ttl                        = 0
  bound_service_account_names      = [data.kubernetes_service_account.vault-secrets-operator-controller-manager.metadata[0].name, "default"]
  bound_service_account_namespaces = [data.kubernetes_namespace_v1.vault.metadata[0].name]
  token_policies                   = local.token_policies
}

resource "helm_release" "transit-auth" {
  name       = "transit-auth"
  repository = "https://dysnix.github.io/charts"
  chart      = "raw"
  namespace  = "vault"
  version    = "v0.3.2"
  values = [
    yamlencode({
      resources = [
        {
          apiVersion = "secrets.hashicorp.com/v1beta1"
          kind       = "VaultAuth"
          metadata = {
            name      = "transit-auth"
            namespace = "vault"
            labels = {
              cacheStorageEncryption = "true"
            }
          }
          spec = {
            method             = "kubernetes"
            mount              = "kubernetes"
            namespace          = "vault"
            vaultConnectionRef = "default"
            storageEncryption = {
              mount   = vault_mount.transit.path
              keyName = vault_transit_secret_backend_key.key.name
            }
            kubernetes = {
              role = vault_kubernetes_auth_backend_role.operator.role_name
              # serviceAccount = "default"
              serviceAccount = data.kubernetes_service_account.vault-secrets-operator-controller-manager.metadata[0].name
              audiences = [
                "vault"
              ]
            }
          }
        }
      ]
    })
  ]
}
