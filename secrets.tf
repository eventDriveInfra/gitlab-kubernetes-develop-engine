resource "vault_mount" "project" {
  path        = var.project.name
  type        = "kv"
  options     = { version = "2" }
  description = "KV Version 2 secret engine mount"
}

resource "vault_kv_secret_v2" "project" {
  for_each  = local.env_data
  mount     = vault_mount.project.path
  name      = each.key
  data_json = jsonencode(each.value)
}
