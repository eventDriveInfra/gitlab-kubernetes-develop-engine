provider "vault" {
  add_address_to_env = true
  address            = var.vault.host
  token              = var.vault.token
}

provider "helm" {
  kubernetes {
    host                   = var.kubernetes.host
    client_certificate     = var.kubernetes.cert
    client_key             = var.kubernetes.key
    cluster_ca_certificate = var.kubernetes.ca
  }
}

provider "kubernetes" {
  host                   = var.kubernetes.host
  client_certificate     = var.kubernetes.cert
  client_key             = var.kubernetes.key
  cluster_ca_certificate = var.kubernetes.ca
}

provider "gitlab" {
  token    = var.gitlab.token
  base_url = var.gitlab.host
}

provider "postgresql" {
  host            = local.postgres["HOST"]
  port            = 5432
  database        = "postgres"
  username        = "postgres"
  password        = local.postgres["INITIAL_ADMIN_PASSWORD"]
  sslmode         = "require"
  connect_timeout = 15
}

data "vault_generic_secret" "minio" {
  path = "minio/data"
}

locals {
  minio = data.vault_generic_secret.minio.data
}

provider "minio" {
  minio_server      = local.minio["API_HOST"]
  minio_user        = local.minio["ADMIN_USER"]
  minio_password    = local.minio["ADMIN_PASSWORD"]
  minio_region      = local.minio["REGION"]
  minio_api_version = "v4"
  minio_ssl         = true
}
