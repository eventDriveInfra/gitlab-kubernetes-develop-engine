variable "developers" {
  type = map(string)
}

variable "components" {
  type = map(object({
    id     = optional(number)
    ports  = set(number)
    secret = optional(bool, true)
    ingress = optional(
      object({
        port        = optional(number)
        path_type   = optional(string, "Prefix")
        host        = optional(bool, false)
        routes      = optional(set(string), ["/"])
        annotations = optional(map(string))
      })
    )
    env = optional(list(string))
  }))
}

variable "project" {
  type = object({
    id         = number
    name       = string
    domain     = string
    namespaces = map(map(map(string)))
  })
}

variable "vault" {
  type = object({
    host              = string
    token             = string
    gitlab_app_id     = string
    gitlab_app_secret = string
  })
}

variable "gitlab" {
  type = object({
    host     = string
    token    = string
    username = string
    email    = string
  })
}

variable "kubernetes" {
  type = object({
    ca   = string
    key  = string
    cert = string
    host = string
  })
}
