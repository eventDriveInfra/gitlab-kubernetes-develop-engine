locals {
  namepsaces = toset([for namespace, env in var.project.namespaces : namespace])
}

resource "minio_s3_bucket" "my-flora" {
  for_each = local.namepsaces
  bucket   = "my-flora-${each.key}"
}

resource "minio_iam_user" "my-flora" {
  for_each = local.namepsaces
  name     = "my-flora-${each.key}"
}

resource "minio_iam_policy" "my-flora" {
  for_each = local.namepsaces
  name     = "my-flora-${each.key}"
  policy = jsonencode(
    {
      Version : "2012-10-17",
      Statement : [
        {
          Sid : "my-flora",
          Effect : "Allow",
          Action : ["s3:*"],
          Resource : "arn:aws:s3:::${minio_s3_bucket.my-flora[each.key].bucket}"
        }
      ]
  })
  lifecycle {
    ignore_changes = [policy]
  }
}

resource "minio_iam_user_policy_attachment" "my-flora" {
  for_each    = local.namepsaces
  user_name   = minio_iam_user.my-flora[each.key].id
  policy_name = minio_iam_policy.my-flora[each.key].id
}

resource "minio_iam_service_account" "my-flora" {
  for_each    = local.namepsaces
  target_user = minio_iam_user.my-flora[each.key].name
  lifecycle {
    ignore_changes = [policy]
  }
}


resource "kubernetes_secret" "minio" {
  for_each = local.namepsaces
  metadata {
    name      = "minio"
    namespace = each.key
  }
  data = {
    MINIO_HOST       = local.minio["IMPLICIT_HOST"]
    MINIO_BUCKET     = minio_s3_bucket.my-flora[each.key].bucket
    MINIO_PORT       = tostring(9000)
    MINIO_SSL        = tostring(false)
    MINIO_ACCESS_KEY = minio_iam_service_account.my-flora[each.key].access_key
    MINIO_SECRET_KEY = minio_iam_service_account.my-flora[each.key].secret_key
  }
}
