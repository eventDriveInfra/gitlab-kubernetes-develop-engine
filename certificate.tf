locals {
  ingress_with_host_names = compact([for key in local.components_with_ingress_keys :
  lookup(var.components, key).ingress.host == true ? key : ""])
}

resource "helm_release" "tls-cert-project" {
  name       = "tls-cert-project"
  repository = "https://dysnix.github.io/charts"
  chart      = "raw"
  version    = "v0.3.2"
  values = [
    yamlencode({
      resources = [
        for ns in keys(var.project.namespaces) :
        {
          apiVersion = "cert-manager.io/v1"
          kind       = "Certificate"
          metadata = {
            name      = "certificate"
            namespace = ns
          }
          spec = {
            secretName = local.tls_secret_name
            issuerRef = {
              name  = "pfile"
              kind  = "ClusterIssuer"
              group = "cert-manager.io"
            }
            dnsNames = concat([ns == local.PRODUCTION ? var.project.domain : "${ns}.${var.project.domain}"],
            [for name in local.ingress_with_host_names : ns == local.PRODUCTION ? "${name}-${var.project.domain}" : "${name}-${ns}.${var.project.domain}"])
          }
        }
      ]
    })
  ]
}
