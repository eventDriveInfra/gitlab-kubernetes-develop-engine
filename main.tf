terraform {
  backend "kubernetes" {
    secret_suffix  = "rbac"
    config_path    = "~/.kube/config"
    config_context = "default"
  }
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.18.0"
    }
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.20.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.6.0"
    }
    minio = {
      source  = "aminueza/minio"
      version = "2.0.1"
    }
  }
}
