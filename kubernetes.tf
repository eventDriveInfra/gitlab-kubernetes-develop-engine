data "kubernetes_config_map" "my_cluster" {
  metadata {
    name      = "kube-root-ca.crt"
    namespace = "kube-system"
  }
}

resource "kubernetes_namespace" "all_namespaces" {
  for_each = var.project.namespaces
  metadata {
    name = each.key
  }
}

resource "kubernetes_service_account" "user_service_accounts" {
  for_each = var.project.namespaces
  metadata {
    namespace = each.key
    name      = each.key
  }
  depends_on = [kubernetes_namespace.all_namespaces]
}

resource "kubernetes_role" "user_namespace_roles" {
  for_each = var.project.namespaces

  metadata {
    name      = each.key
    namespace = kubernetes_namespace.all_namespaces[each.key].metadata[0].name
  }

  rule {
    verbs      = ["get", "list", "watch", "create", "update", "delete", "patch"]
    api_groups = [""]
    resources  = ["deployment", "configmap", "secret"]
  }
}


resource "kubernetes_role_binding" "user_namespace_rolebindings" {
  for_each = var.project.namespaces

  metadata {
    name      = each.key
    namespace = kubernetes_namespace.all_namespaces[each.key].metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.user_service_accounts[each.key].metadata[0].name
    namespace = kubernetes_namespace.all_namespaces[each.key].metadata[0].name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.user_namespace_roles[each.key].metadata[0].name
  }
}

resource "kubernetes_secret" "user_tokens" {
  for_each = var.project.namespaces
  metadata {
    name      = kubernetes_service_account.user_service_accounts[each.key].metadata[0].name
    namespace = kubernetes_namespace.all_namespaces[each.key].metadata[0].name

    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account.user_service_accounts[each.key].metadata[0].name
    }
  }

  type = "kubernetes.io/service-account-token"

  depends_on = [kubernetes_service_account.user_service_accounts]
}

locals {
  kube_config_template = <<-EOF
apiVersion: v1
kind: Config
clusters:
- name: default
  cluster:
    certificate-authority-data: %s # Здесь подставится CA-сертификат
    server: %s # Здесь подставится адрес API-сервера
contexts:
%s
current-context: %s
users:
%s
EOF
}

resource "local_file" "kubeconfig_files" {
  for_each = var.developers

  filename = "kubeconfig-${each.key}"
  content = format(
    local.kube_config_template,
    base64encode(data.kubernetes_config_map.my_cluster.data["ca.crt"]),
    var.kubernetes.host,
    join("\n", [for namespace, data in var.project.namespaces :
      format("- name: %s\n  context:\n    cluster: default\n    namespace: %s\n    user: %s",
    namespace, namespace, namespace)]),
    " ",
    join("\n", [for key, namespace in var.project.namespaces :
      format("- name: %s\n  user:\n    token: %s",
    key, kubernetes_secret.user_tokens[key].data["token"])])
  )
}
