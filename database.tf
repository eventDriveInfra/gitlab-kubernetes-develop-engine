resource "postgresql_role" "project" {
  for_each = { for name, data in var.project.namespaces : name => data if can(data.database.username) }
  name     = each.value.database.username
  login    = true
  password = each.value.database.password
}

resource "postgresql_database" "project" {
  for_each = var.project.namespaces
  name     = each.value.database.name
  owner    = can(postgresql_role.project[each.key]) ? postgresql_role.project[each.key].name : "postgres"
}

resource "postgresql_database" "project-shadow" {
  for_each = var.project.namespaces
  name     = "${each.value.database.name}-shadow"
  owner    = can(postgresql_role.project[each.key]) ? postgresql_role.project[each.key].name : "postgres"
}
