resource "kubernetes_service_v1" "services" {
  for_each = { for pair in setproduct(toset([
    for key, value in var.components : {
      name  = key
      ports = value.ports
    }
    ]), keys(var.project.namespaces)) :
    "${pair[0].name}-${pair[1]}" => {
      name      = pair[0].name
      ports     = pair[0].ports
      namespace = pair[1]
  } }
  metadata {
    name      = each.value.name
    namespace = each.value.namespace
  }
  spec {
    selector = {
      "app" = each.value.name
    }
    dynamic "port" {
      for_each = each.value.ports
      content {
        name = tostring(port.value)
        port = port.value
      }
    }
  }
}

locals {
  tls_secret_name = "certificate"
  components_with_ingress_keys = compact([for key, component in var.components :
  component.ingress != null ? key : ""])
}

resource "kubernetes_ingress_v1" "ingresses" {
  depends_on = [helm_release.tls-cert-project]
  for_each = { for pair in setproduct([
    for key in local.components_with_ingress_keys : merge(lookup(var.components, key), { name = key })],
    keys(var.project.namespaces)) : "${pair[0].name}-${pair[1]}" => {
    ingress   = pair[0].ingress
    name      = pair[0].name
    ports     = pair[0].ports
    namespace = pair[1]
    }
  }
  metadata {
    name        = each.value.name
    namespace   = each.value.namespace
    annotations = each.value.ingress.annotations
  }
  spec {
    ingress_class_name = "nginx"
    tls {
      hosts = [
        "${each.value.namespace == local.PRODUCTION ?
          (each.value.ingress.host ? "${each.value.name}.${var.project.domain}" : var.project.domain) :
          (each.value.ingress.host ? "${each.value.name}-${each.value.namespace}" : each.value.namespace)
        }${each.value.namespace != local.PRODUCTION ? ".${var.project.domain}" : ""}"
      ]
      secret_name = local.tls_secret_name
    }
    rule {
      host = "${each.value.namespace == local.PRODUCTION ?
        (each.value.ingress.host ? "${each.value.name}.${var.project.domain}" : var.project.domain) :
        (each.value.ingress.host ? "${each.value.name}-${each.value.namespace}" : each.value.namespace)
      }${each.value.namespace != local.PRODUCTION ? ".${var.project.domain}" : ""}"
      http {
        dynamic "path" {
          for_each = each.value.ingress.routes
          content {
            path      = path.value
            path_type = each.value.ingress.path_type
            backend {
              service {
                name = each.value.name
                port {
                  number = each.value.ingress.port != null ? each.value.ingress.port : coalesce(each.value.ports...)
                }
              }
            }
          }
        }
      }
    }
  }
}
