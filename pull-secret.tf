resource "kubernetes_secret_v1" "pull-secret" {
  for_each = var.project.namespaces
  metadata {
    name      = "pull-secret"
    namespace = kubernetes_namespace.all_namespaces[each.key].metadata[0].name
  }
  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "registry.gitlab.com" = {
          "email" = var.gitlab.email
          "auth"  = base64encode("${var.gitlab.username}:${var.gitlab.token}")
        }
      }
    })
  }
  type = "kubernetes.io/dockerconfigjson"
}
