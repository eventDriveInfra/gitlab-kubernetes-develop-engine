locals {
  gitlab_vars = {
    "TF_VAR_kubernetes" = jsonencode(var.kubernetes)
    "TF_VAR_components" = jsonencode(var.components)
    "TF_VAR_vault"      = jsonencode(var.vault)
  }
}

resource "gitlab_project_variable" "my-flora" {
  for_each  = local.gitlab_vars
  project   = var.project.id
  key       = each.key
  value     = each.value
  protected = true
}
