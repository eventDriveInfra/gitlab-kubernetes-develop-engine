data "kubernetes_namespace_v1" "vault" {
  metadata {
    name = "vault"
  }
}

resource "kubernetes_secret_v1" "vault" {
  metadata {
    name      = "vault-token-g955r"
    namespace = data.kubernetes_namespace_v1.vault.metadata[0].name
    annotations = {
      "kubernetes.io/service-account.name" = "vault"
    }
  }
  type = "kubernetes.io/service-account-token"
}

# Активация Kubernetes Auth Method в Vault
resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
  path = "kubernetes"
}

# Настройка Kubernetes Auth Method в Vault
resource "vault_kubernetes_auth_backend_config" "kubernetes" {
  kubernetes_host    = var.kubernetes.host
  backend            = vault_auth_backend.kubernetes.path
  token_reviewer_jwt = lookup(kubernetes_secret_v1.vault.data, "token")
  kubernetes_ca_cert = data.kubernetes_config_map.my_cluster.data["ca.crt"]
}

# Создание Vault политик и привязка к ServiceAccount по умолчанию для каждого пространства имен
resource "vault_policy" "user_namespace_policies" {
  for_each = var.project.namespaces

  name   = "user-policy-${each.key}"
  policy = <<EOF
path "${vault_mount.project.path}/*" {
  capabilities = ["create", "read", "update", "patch", "delete", "list"]
}
path "${vault_mount.storage.path}/creds/${vault_database_secret_backend_role.project[each.key].name}" {
  capabilities = ["read", "list"]
}
EOF

  depends_on = [kubernetes_service_account.user_service_accounts]
}

resource "vault_kubernetes_auth_backend_role" "user_namespace_auth_roles" {
  for_each = var.project.namespaces

  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = each.key
  bound_service_account_names      = [kubernetes_service_account.user_service_accounts[each.key].metadata[0].name]
  bound_service_account_namespaces = [kubernetes_namespace.all_namespaces[each.key].metadata[0].name]
  token_policies                   = [vault_policy.user_namespace_policies[each.key].name]
}

resource "vault_auth_backend" "gitlab" {
  type = "oidc"
  path = "oidc"
  tune {
    max_lease_ttl      = "90000s"
    listing_visibility = "unauth"
  }
}
